const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;

const BookSchema = mongoose.Schema({
    primary_isbn: {
        type: String
    },
    title: {
        type: String,
    },
    bookkey: {
        type: String,
    },
    price: {
        type: String,
    },
    publisher: {
        type: String,
    },
    active: {
        type: Boolean,
    },
    image: {
        type: String,
    },
    
    authors :[
    {
    role:String,
    short_bio:String,
    firebrand_role:String,
    firebrand_id:String,
    display_name:String,
    first_name:String,
    last_name:String,
    slug:String,
    }
    ],
    isbns : { type : Array , "default" : [] },
    primary_bisacs : { type : Array , "default" : [] },
});

const Book = mongoose.model('Book', BookSchema);

module.exports = Book;
