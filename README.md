## Introduction: 
This a nodejs-reactjs application for managing book list. It currently uses MongoDB for the database and Nodejs for the REST API server.

## Prerequisite: 
1.	Nodejs v6+.
2.	Mongodb (Latest).

## How to run: 
Ensure you have mongodb installed in your system and that it is running
•	Go to client folder and run `npm run build`.
•	Now after complete build run this build by command `serve -s build`.
•	Now we our reactjs front end running successfully.
•	TO start our backed move back from client folder to root location of project.
•	Now run `npm run start`.
•	This will start our backed server.
•	Now hit on utl <http://localhost:3000> and application run successfully.

## What this application does: 

•	On home page show all book list.
•	React pagination is used in this page.
•	Filter can also apply for books by status and title.
•	Book detail page created for full detail of book.
