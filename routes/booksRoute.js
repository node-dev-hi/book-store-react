const express = require('express');

const Book = require('../models/bookModel.js');
let router = express.Router();

router.post('/', (req, res) => {
    // console.log(req.body);
    var conditions = {};
    var and_clauses = [];
    if (req.body.title) {
        and_clauses.push({ 'title': { $regex: req.body.title, $options: 'i' } });
    }
    if (req.body.status) {
        if (req.body.status=='Active') {
            and_clauses.push({ 'active': true});
        }else{
            and_clauses.push({ 'active': false});
        }
    }
  
    if (and_clauses.length > 0) {
        conditions['$and'] = and_clauses;
    }
    pageSize = 10
    page_num = req.body.page
    skips = pageSize * (page_num - 1)
    Book.find(conditions).countDocuments().exec(function (err, response) {
        if(response>0){
            Book.find(conditions, {_id:1,title:1,price:1,active:1,publisher:1,primary_isbn:1}).skip(skips).limit(pageSize).exec(function (err, books) {
                res.json({ 'count': response,'books':books });
            });
        }else{
            res.json({ 'count': response,'books':[] });
        }
    });
});

router.get('/:id', (req, res) => {
    Book.findById(req.params.id, (err, book) => {
        if (err) throw err;
        res.json({ book });
    })
});

module.exports = router;
