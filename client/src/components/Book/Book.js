import React from 'react';
import './Book.css';
import WrappedLink from '../WrappedLink/WrappedLink';

const book = (props) => {
    return (
        <tr>
            <td>
                {props.primary_isbn}
            </td>
            <td>
                <strong>{props.title}</strong>
            </td>
            <td>
                {props.publisher}
            </td>
            <td>
                $ {props.price}
            </td>
            <td>
                <b>{props.active ? 'Active' : 'Inactive'}</b>
                
            </td>
            <td><WrappedLink
                to={'/books/' + props.id}
                buttonClasses={['btn', 'btn-info', 'ViewButton']}>View</WrappedLink>
                </td>
        </tr>
    );
}

export default book;
