import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getBook } from '../../../store/actions/booksActions';
import WrappedLink from '../../../components/WrappedLink/WrappedLink';
import ReactHtmlParser from 'react-html-parser';
import './FullBook.css'

class FullBook extends Component {
    componentDidMount() {
        this.getSingleBook();
    }

    getSingleBook() {
        if (this.props.match.params.id) {
            if (!this.props.book || (this.props.book._id !== + this.props.match.params.id)) {
                this.props.getBook(this.props.match.params.id);
            }
        }
    }

    handleBookListClick() {
        this.props.history.replace({ pathname: '/' });
    }

    render() {

        var allauthors = "";
        if (this.props.book.authors) {
            allauthors = this.props.book.authors.map((item) =>
                <div key={item._id}>{item.display_name}</div>
            );
        }

        return (

            <div className="container">
                <br />
                <div className="jumbotron FullBook">
                    <h3 className="text-center">{this.props.book.title}</h3>
                    <div className="row">
                        <div className="col-sm-6">
                            <p><strong>ISBN:</strong> {this.props.book.primary_isbn}</p>
                            <p><strong>Price:</strong> $ {this.props.book.price}</p>
                            <p><strong>Genre:</strong> {this.props.book.genre}</p>
                            <p><strong>Publisher:</strong> {this.props.book.publisher}</p>
                            <p><strong>Bisacs:</strong> {this.props.book.primary_bisacs}</p>
                        </div>
                        <div className="col-sm-6">
                            <h5 className="text-right">- Authors
                            {allauthors}
                            </h5>
                        </div>
                    </div>
                    {this.props.book.description ?
                        <div className="description">{ReactHtmlParser(this.props.book.description)}</div>
                        : ''
                    }
                    <WrappedLink
                        to={"/"}
                        buttonClasses={['btn', 'btn-info', 'mr-2']}
                        click={() => this.handleBookListClick()}>Back</WrappedLink>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        book: state.books.book
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getBook: (bookId) => dispatch(getBook(bookId))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FullBook);
