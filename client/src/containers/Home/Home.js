import React, { Component } from 'react';
import { connect } from 'react-redux';
import Pagination from "react-js-pagination";
import { getAllBooks } from '../../store/actions/booksActions';
import Book from '../../components/Book/Book';

import './Home.css';

class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            bookfilter: {},
            activePage: 1
        };
        this.handlePageChange = this.handlePageChange.bind(this);
    }
    
    handlePageChange(pageNumber) {
        this.setState((prevState) => {
            return {
                ...prevState,
                activePage:pageNumber
            };
        });
        this.props.initBooks({...this.state.bookfilter,page:pageNumber});
    }

    componentDidMount() {
        this.props.initBooks({page:this.state.activePage});
    }

    handleInputChange = (e) => {
       
        const field = e.target.name;
        const value = e.target.value;

        this.setState((prevState) => {
            return {
                ...prevState,
                bookfilter: {
                    ...prevState.bookfilter,
                    [field]: value
                },
            };
        });
    }
    
    handleFilterForm = (e) => {
        e.preventDefault();
        
        this.setState((prevState) => {
            return {
                ...prevState,
                activePage:1
            };
        });
        this.props.initBooks({...this.state.bookfilter,page:1});

    }
    render() {
        let allBooks = this.props.allBooks;
        allBooks = allBooks.map(book => (
            <Book
                key={book._id}
                id={book._id}
                title={book.title}
                primary_isbn={book.primary_isbn}
                publisher={book.publisher}
                active={book.active}
                price={book.price} />
        ));

        return (

            <div className="container">
                <br />
                <div className="row">
                    <div className="col-sm-6">
                        <div className="Header">
                            <h1 style={{ display: 'inline-block' }}>All Books</h1>
                        </div>
                    </div>
                    <div className="col-sm-6 pull-right">
                        <form className="form-inline " onSubmit={this.handleFilterForm.bind(this)}>
                            <div className="form-group mr-5 pull-right">
                                <input type="text" name="title" onChange={this.handleInputChange.bind(this)} placeholder="Title" className="form-control" />
                            </div>
                            <div className="form-group mr-5 pull-right" onChange={this.handleInputChange.bind(this)}>
                                <select name="status" className="form-control">
                                    <option value="">Status</option>
                                    <option value="Active">Active</option>
                                    <option value="Inactive">Inactive</option>
                                </select>
                            </div>
                            <div className="form-group ml-2">
                                <button type="submit" className="btn btn-success">Search</button>
                            </div>
                        </form>
                    </div>
                </div>
                <br />
                <div>
                    <section className="section">
                        <div className="Books">
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th>ISBN</th>
                                        <th>Title</th>
                                        <th>Publisher</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {allBooks}
                                </tbody>
                            </table>

                        </div>
                        <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={10}
                            totalItemsCount={this.props.bookcount}
                            pageRangeDisplayed={5}
                            itemClass="page-item"
                            linkClass="page-link"
                            onChange={this.handlePageChange}
                      />
                    </section>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        allBooks: state.books.books,
        bookcount: state.books.bookcount,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        initBooks: (bookData) => dispatch(getAllBooks(bookData)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
