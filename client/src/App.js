import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './containers/Home/Home';
import FullBook from './containers/Books/FullBook/FullBook';

class App extends Component {
    render() {
        return (
            <div className="container-fluid">
                <Switch>
                    <Route path="/books/:id" component={FullBook} />
                    <Route path="/" component={Home} />
                </Switch>
            </div>
        );
    }
}

export default App;
