import * as actionTypes from './actionTypes';

const options = (data) => {
    return {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        method: 'post',
        body: JSON.stringify(data)
    };
};

export const getAllBooks = (bookdata) => {
    console.log('-------------------------',bookdata);
    return dispatch => {
        fetch('/api/books', options(bookdata))
        .then(res => res.json())
        .then(res => {
            dispatch({ type: actionTypes.GOT_ALL_BOOKS, books: res.books, count: res.count })
        })
    };
};


export const getBook = (bookId) => {
    return dispatch => {
        fetch('/api/books/' + bookId)
        .then(res => res.json())
        .then(res => {
            // console.log(res.book);
            dispatch({ type: actionTypes.GOT_SINGLE_BOOKS, book: res.book })
        })
    };
};

export const submitNewBook = (bookData) => {
    return dispatch => {
        return fetch('/api/books/add', options(bookData))
        .then(res => res.json())
    }
};


