import * as actionTypes from '../actions/actionTypes';

const initialState = {
    books: [],
    book: {},
    bookcount: 0,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GOT_ALL_BOOKS:
            return {
                ...state,
                books: action.books,
                bookcount: action.count,
            };
        case actionTypes.GOT_SINGLE_BOOKS:
            return {
                ...state,
                book: action.book
            };
        default:
            return state;
    }
};

export default reducer;
